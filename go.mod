module gitee.com/butnet/zip

go 1.19

require (
	github.com/yeka/zip v0.0.0-20180914125537-d046722c6feb
	golang.org/x/crypto v0.6.0
)
